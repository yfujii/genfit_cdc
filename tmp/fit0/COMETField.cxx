/*
   Authors: Yuki FUJII
   Call the field which is used in ICEDUST
*/
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <sstream>
#include <fstream>

#include "COMETField.h"
#include "IBLFieldMap.hh"
#include "G4String.hh"

const double kGaus = 1e4;

namespace genfit {

void COMETField::setFieldMap(G4String filename, double scaleFactor) {
  fFieldMap    = new COMET::IBLFieldMap();
  fFileName    = filename;
  fScaleFactor = scaleFactor;

  if (fFieldMap->readFile(fFileName)) {
    fConstantField = false;
  } else {
    fConstantField = true;
  }
}

void COMETField::setConstantField(double bx, double by, double bz) {
  fConstantBx = bx;
  fConstantBy = by;
  fConstantBz = bz;
}

void COMETField::setOffsetPosition(double x, double y, double z) {
  fOffsetPosition = TVector3(x, y, z);
}

void COMETField::setRotationVector(double rotx, double roty, double rotz) {
  fRotationVector = TVector3(rotx, roty, rotz);
}

TVector3 COMETField::get(const TVector3& pos) const {
  if (!fConstantField) {
    G4double local[4] = {pos.X(), pos.Y(), pos.Z(), 0};
    G4double field[6];
    G4double ftrans[3];
    G4double global[4];
    // rotate -90 degree around Y-axis
    global[0] = (+local[2] + fOffsetPosition.X()) * 10.; // cm -> mm
    global[1] = (+local[1] + fOffsetPosition.Y()) * 10.; // cm -> mm
    global[2] = (-local[0] + fOffsetPosition.Z()) * 10.; // cm -> mm
    global[3] = +local[4];
    fFieldMap->getFieldValue(global, field);
    ftrans[0] = fScaleFactor*field[2]*kGaus;
    ftrans[1] = fScaleFactor*field[1]*kGaus;
    ftrans[2] = fScaleFactor*field[0]*kGaus;
    return TVector3(ftrans[0], ftrans[1], ftrans[2]);
  } else {
    return TVector3(fConstantBx, fConstantBy, fConstantBz);
  }
}

void COMETField::get(const double& xpos, const double& ypos, const double& zpos, double& Bx, double& By, double& Bz) const {
  if (!fConstantField) {
    G4double local[4] = {xpos, ypos, zpos, 0};
    G4double field[6];
    G4double global[4];
    // rotate -90 degree around Y-axis
    global[0] = (+local[2] + fOffsetPosition.X()) * 10.; // cm -> mm
    global[1] = (+local[1] + fOffsetPosition.Y()) * 10.; // cm -> mm
    global[2] = (-local[0] + fOffsetPosition.Z()) * 10.; // cm -> mm
    global[3] = +local[4];
    fFieldMap->getFieldValue(global, field);
    Bx = fScaleFactor * field[2] * kGaus;
    By = fScaleFactor * field[1] * kGaus;
    Bz = fScaleFactor * field[0] * kGaus;
  } else {
    Bx = fScaleFactor * fConstantBx;
    By = fScaleFactor * fConstantBy;
    Bz = fScaleFactor * fConstantBz;
  }
}

} /* End of namespace genfit */
