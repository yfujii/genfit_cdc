/*
   Authors: Yuki FUJII
   Call the field which is used in ICEDUST
*/

#ifndef genfit_COMETField_h
#define genfit_COMETField_h

#include "AbsBField.h"
#include "G4String.hh"

namespace COMET {
  class IBLFieldMap;
}

namespace genfit {

class COMETField : public AbsBField {
  /// nested class to store parameters for a fieldmap
 public:
  //! define the constant field in this ctor
  COMETField(/*double b1, double b2, double b3*/)
    : //field_(b1, b2, b3),
      fFieldMap(0),
      fFileName(""),
      fScaleFactor(1.0),
      fConstantBx(0.),
      fConstantBy(0.),
      fConstantBz(-10.0), // default -10 kGaus
      fConstantField(false),
      fOffsetPosition(TVector3(0,0,0)),
      fRotationVector(TVector3(0,0,0)) {
  }

  //COMETField(const TVector3& field)
  //  : field_(field)
  //{ ; }

  //! return value at position
  TVector3 get(const TVector3& pos) const;
  void get(const double& posX, const double& posY, const double& posZ, double& Bx, double& By, double& Bz) const;
  void setFieldMap(G4String filename, double scaleFactor = 1.0);
  void setConstantField(double bx, double by, double bz);
  void setOffsetPosition(double x, double y, double z);
  void setRotationVector(double rotx, double roty, double rotz);
  G4String GetFileName() {return fFileName;};
  G4String GetScaleFactor() {return fScaleFactor;};

 private:
  //TVector3 field_;
  COMET::IBLFieldMap *fFieldMap;
  G4String fFileName;
  double   fScaleFactor;
  double   fConstantBx;
  double   fConstantBy;
  double   fConstantBz;
  bool     fConstantField;
  TVector3 fOffsetPosition;
  TVector3 fRotationVector;
};

} /* End of namespace genfit */

#endif // genfit_COMETField_h
