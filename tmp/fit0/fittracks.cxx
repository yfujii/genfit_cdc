#include "TROOT.h"
#include "TStyle.h"
#include "TApplication.h"
#include "TRint.h"
#include "TGeoManager.h"
#include "TRandom.h"
#include "TFile.h"
#include "TTree.h"
#include "TMath.h"
#include "TGeoTube.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TF1.h"
#include "TBranch.h"
#include "TGraph.h"
#include "TCanvas.h"
#include "TChain.h"

#include <Exception.h>
#include <AbsTrackRep.h>
#include <RKTrackRep.h>
#include <ConstField.h>
#include <COMETField.h>
#include <FieldManager.h>
#include <TGeoMaterialInterface.h>
#include <MaterialEffects.h>

#include <WireMeasurement.h>
#include <WirePointMeasurement.h>
#include <SpacepointMeasurement.h>
#include <ProlateSpacepointMeasurement.h>
#include <Track.h>
#include <KalmanFittedStateOnPlane.h>
#include <AbsKalmanFitter.h>
#include <KalmanFitter.h>
#include <KalmanFitterRefTrack.h>
#include <KalmanFitterInfo.h>
#include <DAF.h>
#include <KalmanFitStatus.h>
#include <EventDisplay.h>
#include "math.h"

#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <getopt.h>

#include "recEvent.h"

///////////////////////////////////
// data for the analysis...
///////////////////////////////////
  Int_t    eventID   = 0; // event ID
  Double_t rsmear    = 0; // smearing value in um
  Double_t iniposZ   = 0; // at the target (given)
  Double_t inimomX   = 0; // at the target (MC)
  Double_t inimomY   = 0; // at the target (MC)
  Double_t inimomZ   = 0; // at the target (MC)
  Int_t nHits        = 0; // #of total hits
  Int_t nHits1st     = 0; // #of hits in 1st turn
  Int_t nTurns       = 0; // #of turns
  Int_t nLayers      = 0; // #of layers
  Int_t statusCut    = 0; // cut status (0: not selected, 1: selected)
  Int_t statusCTH    = 0; // cth status (0: no hit, 1: hit, 2: indirect hit)
  Int_t statusFit    = 0; // fit status (0: not fitted, 1: fitted)
  Int_t nHitsFitted  = 0; // #of hits used for the fitting
  Double_t chi2ndf   = 0; // chi2/ndf
  Double_t pvalue    = 0; // p-value
  std::vector <double>  mcposX; // X hit point (MC)
  std::vector <double>  mcposY; // Y hit point (MC)
  std::vector <double>  mcposZ; // Z hit point (MC)
  std::vector <double>  mcposD; // D hit point (MC)
  std::vector <double>  mcmomX; // mom X hit point (MC)
  std::vector <double>  mcmomY; // mom Y hit point (MC)
  std::vector <double>  mcmomZ; // mom Z hit point (MC)
  std::vector <double>  recposX; // X hit point (reconstructed)
  std::vector <double>  recposY; // Y hit point (reconstructed)
  std::vector <double>  recposZ; // Z hit point (reconstructed)
  std::vector <double>  recposD; // D hit point (reconstructed)
  std::vector <double>  recmomX; // mom X hit point (reconstructed)
  std::vector <double>  recmomY; // mom Y hit point (reconstructed)
  std::vector <double>  recmomZ; // mom Z hit point (reconstructed)
  std::vector <double>  recposXCov; // Cov for X
  std::vector <double>  recposYCov; // Cov for Y
  std::vector <double>  recposZCov; // Cov for Z
  std::vector <double>  recmomXCov; // Cov for p_x
  std::vector <double>  recmomYCov; // Cov for p_y
  std::vector <double>  recmomZCov; // Cov for p_z
  std::vector <double>  recposXtrue; // X hit point for true track
  std::vector <double>  recposYtrue; // Y hit point for true track
  std::vector <double>  recposZtrue; // Z hit point for true track
  std::vector <double>  recposDtrue; // D hit point for true track
  std::vector <double>  recmomXtrue; // mom X hit point for true track
  std::vector <double>  recmomYtrue; // mom Y hit point for true track
  std::vector <double>  recmomZtrue; // mom Z hit point for true track
///////////////////////////////////

TFile *inFile;
TTree *inTree;
TTree *outTree;

TH1D *momthist;
TH1D *momres;
TH1D *momtres;
TH1D *momzres;
TH1D *chi2hist;
TH1D *pvalhist;
TH1D *driftDdiff;

TH1D *momres_20_40;
TH1D *momres_40_50;
TH1D *momres_50_60;
TH1D *momres_60_70;
TH1D *momres_70_80;
TH1D *momres_80_90;
TH1D *Nhithist;
TH1D *driftDfittedhist;
TH2D *driftDvsdriftres;

TFile* outputfile;

int startZ; // target z position
int smearR; // r smearing value

int nWires; // number of total wires
std::vector < TF1* >     fXYWire;  // wire function
std::vector < TVector3 > wirePos;  // wire position @ z=0
std::vector < TVector3 > wirePos0; // wire position @ downstream endplate
std::vector < TVector3 > wirePos1; // wire position @ upstream   endplate

using namespace genfit;

// constants
const double deg2rad        = TMath::DegToRad();
const double two_pi         = TMath::TwoPi();
const double half_pi        = TMath::Pi()/2.;
// gas simulation
//const double edep2ncls      = 1./(28.7*eV); // (#of clusters)/(energy deposit [eV]) from Garfield++
// cdc geometry
const double centimeter     = 1.;
const double micrometer     = 1e-04;
const double lenOffset      = 129.48*centimeter; // (Offset length of CDC)
const double tiltAngle      = 10.*deg2rad;
const double rminLayer      = 52.2*centimeter;   // minimum radius of sense layer in [cm]
const double hCell          = 1.6*centimeter;    // height of cell @EP in [cm]
const int    nLayer         = 18;
const int    nWire0         = 396/2;             // #of wires @ 0-th layer
const int    nDeltaW        = 12/2;              // step of the #of wires
const int    innerskipHole  = 12/2;              // #of skip holes @EP to get the stereo angle
const int    outerskipHole  = 14/2;              // #of skip holes @EP to get the stereo angle
const double rinCDC         = 50.0*centimeter;
const double routCDC        = 81.5*centimeter;
const int    pdg   = 11; // electron
const double dPVal = 1.E-3;

TVector3 xyz0,mom0;
std::vector<StrawHit> hits;

//EventDisplay* display = EventDisplay::getInstance();

int  GetNumberOfWirePerLayer(int ilayer);
void GetWirePosition (int iwire, double *xy, double zpos);

// wire geometry function
double funcXYWire (double *x, double *par){
  // x -- x value
  // par[0] -- a_x
  // par[1] -- a_y
  // par[2] -- x_0
  // par[3] -- y_0
  double val = par[1]/par[0]*(x[0]-par[2]) + par[3];
  return val;
}

void initfitter(){
  std::cout << "//----------------------------------//" << std::endl;
  std::cout << "// Init.  Fitter...                 //" << std::endl;
  std::cout << "//----------------------------------//" << std::endl;

  // Initialization for the magnetic field
  bool useCOMETField = false;
  //bool useCOMETField = true;
  if (useCOMETField) {
    COMETField *field = new COMETField();
    char *fieldPath = getenv("ICEDUST_FIELDMAPS");
    G4String fileName = fieldPath;
    fileName += "/../fields_IHEP/ihep_bs1+det.map";
    double scaleFactor = 1.0; // ???
    field->setFieldMap(fileName, scaleFactor);
    field->setOffsetPosition(641.0*centimeter, 0.0 , 765.65*centimeter);
    field->setRotationVector(0., -TMath::Pi()/2., 0.);

    //// show field values @ several points for debugging
    /*TVector3 fieldOrig1    = field->get(TVector3(0., 0., 0.));
    TVector3 field50plus1  = field->get(TVector3(50., 0., 0.));
    TVector3 field50minus1 = field->get(TVector3(-50., 0., 0.));
    std::cout << fieldOrig1.X()    << " " << fieldOrig1.Y()    << " " << fieldOrig1.Z()    << std::endl;
    std::cout << field50minus1.X() << " " << field50minus1.Y() << " " << field50minus1.Z() << std::endl;
    std::cout << field50plus1.X()  << " " << field50plus1.Y()  << " " << field50plus1.Z()  << std::endl;
    TVector3 fieldOrig2    = field->get(TVector3(0., 0., 100.));
    TVector3 field50plus2  = field->get(TVector3(50., 0., 100.));
    TVector3 field50minus2 = field->get(TVector3(-50., 0., 100.));
    std::cout << fieldOrig2.X()    << " " << fieldOrig2.Y()    << " " << fieldOrig2.Z()    << std::endl;
    std::cout << field50minus2.X() << " " << field50minus2.Y() << " " << field50minus2.Z() << std::endl;
    std::cout << field50plus2.X()  << " " << field50plus2.Y()  << " " << field50plus2.Z()  << std::endl;
    TVector3 fieldOrig3    = field->get(TVector3(0., 0., -100.));
    TVector3 field50plus3  = field->get(TVector3(50., 0., -100.));
    TVector3 field50minus3 = field->get(TVector3(-50., 0., -100.));
    std::cout << fieldOrig3.X()    << " " << fieldOrig3.Y()    << " " << fieldOrig3.Z()    << std::endl;
    std::cout << field50minus3.X() << " " << field50minus3.Y() << " " << field50minus3.Z() << std::endl;
    std::cout << field50plus3.X()  << " " << field50plus3.Y()  << " " << field50plus3.Z()  << std::endl;*/

    FieldManager::getInstance()->init(field); // kGauss
  } else {
    FieldManager::getInstance()->init(new ConstField(0.,0.,10.0)); // kGauss
  }

  // Set material effects
  MaterialEffects* mateff=MaterialEffects::getInstance();
  //mateff->setEnergyLossBetheBloch(true);
  //mateff->setNoiseBetheBloch(true);
  //mateff->setNoiseBetheBloch(true);
  //mateff->setEnergyLossBetheBloch(false);
  //mateff->setNoiseBetheBloch(false);
  mateff->setEnergyLossBrems(false); // turn off same as done in Sakamoto-san's fitting
  //mateff->setMscModel("GEANE"); // WHAT IS GEANE ???? Should be checked !!!!

  MaterialEffects::getInstance()->init(new TGeoMaterialInterface());

  // Initialization for the Wire Configuration Start from HERE
  nWires = 0;
  for (int iLayer = 0; iLayer < nLayer; iLayer++){
    nWires += GetNumberOfWirePerLayer(iLayer);
  }
  fXYWire.resize(nWires);
  wirePos.resize(nWires);
  wirePos0.resize(nWires);
  wirePos1.resize(nWires);

  int wirenum = 0;
  int numWirePerLayer;
  double zpos_wire; // @ EP
  double rpos_wire; // @ EP
  double phi_wire;
  double deltaPhi;
  double alphaAngle; // projection of stereo angle @ XY-plane
  double xEnd[2] = {0,0};
  double yEnd[2] = {0,0};
  double zEnd[2] = {0,0};

  for(int iLayer = 0; iLayer < nLayer; iLayer++){
    //std::cout << iLayer << "-th layer" << std::endl;

    if (iLayer>0) wirenum += GetNumberOfWirePerLayer(iLayer-1);
    numWirePerLayer = GetNumberOfWirePerLayer(iLayer);
    deltaPhi = two_pi/((double)numWirePerLayer);
    if (iLayer<14)  alphaAngle = (iLayer%2!=0)? deltaPhi*((double)innerskipHole) : (-deltaPhi*((double)innerskipHole));
    if (iLayer>=14) alphaAngle = (iLayer%2!=0)? deltaPhi*((double)outerskipHole) : (-deltaPhi*((double)outerskipHole));
    rpos_wire  = rminLayer + (((double)iLayer)+0.5)*hCell;
    zpos_wire  = rpos_wire*tan(tiltAngle) + lenOffset/2.;

    //std::cout << " #of sense wire = " << numWirePerLayer << ", z length = " << 2.*zpos_wire << std::endl;

    int wireID;
    double a_x, a_y, x_0, y_0; // parameters for the wire function
    double minX, maxX;

    for(int icell = 0; icell < numWirePerLayer; icell++){
      wireID = wirenum + icell;
      double PhiOffset = 0;
      if(iLayer==0||iLayer==1||iLayer==3||iLayer==6||iLayer==8||iLayer==10||iLayer==12||iLayer==16){
        PhiOffset = deltaPhi/2.;
      }

      phi_wire = icell * deltaPhi + PhiOffset;
      xEnd[0]  = rpos_wire * cos(phi_wire);
      yEnd[0]  = rpos_wire * sin(phi_wire);
      zEnd[0]  = zpos_wire;
      xEnd[1]  = rpos_wire * cos(phi_wire+alphaAngle);
      yEnd[1]  = rpos_wire * sin(phi_wire+alphaAngle);
      zEnd[1]  = -zpos_wire;
      wirePos0[wireID] = TVector3(xEnd[0], yEnd[0], zEnd[0]);
      wirePos1[wireID] = TVector3(xEnd[1], yEnd[1], zEnd[1]);
      wirePos[wireID]  = TVector3((xEnd[0]+xEnd[1])/2., (yEnd[0]+yEnd[1])/2., 0);
      a_x      = (xEnd[1]-xEnd[0])/(zEnd[1]-zEnd[0]);
      a_y      = (yEnd[1]-yEnd[0])/(zEnd[1]-zEnd[0]);
      x_0      = wirePos[wireID].X();
      y_0      = wirePos[wireID].Y();
      minX     = std::min(xEnd[0],xEnd[1]);
      maxX     = std::max(xEnd[0],xEnd[1]);
      fXYWire[wireID] = new TF1(Form("fXYWire_%d",wireID), funcXYWire, minX, maxX, 4);
      fXYWire[wireID]->SetNpx(2000);
      fXYWire[wireID]->SetParameters(a_x, a_y, x_0, y_0);
    }
  }
}

/// Number of sense wire at i-th layer
int GetNumberOfWirePerLayer(int ilayer){
  return (nWire0+ilayer*nDeltaW);
}

/// Number of sense wire at i-th layer
int GetWireNumber(int ilayer, int icell){
  int wirenum = 0;
  for (int i = 0; i < nLayer; i++) {
    if (i==ilayer) {
       wirenum += icell;
       break;
    }
    wirenum += GetNumberOfWirePerLayer(i);
  }
  return wirenum;
}

/// Find the layer ID from hit position
int FindLayer(TVector3 pos){ // return the number of sense wire (assuming almost square cell shape)
  int layerID = -1;
  int wirenum[nLayer];
  int rpos_wire[nLayer];
  double xy[2];
  for (int iLayer = 0; iLayer < nLayer; iLayer++) {
    if (iLayer>0) wirenum[iLayer] = wirenum[iLayer-1]+GetNumberOfWirePerLayer(iLayer-1);
    else wirenum[iLayer] = 0;
    GetWirePosition(wirenum[iLayer], xy, pos.Z());
    rpos_wire[iLayer] = hypot(xy[0], xy[1]);
  }
  for (int iLayer = 0; iLayer < nLayer; iLayer++) {
    if (pos.Perp()<rpos_wire[iLayer]) {
      layerID = iLayer;
      break;
    } else if (iLayer<nLayer-1 && fabs(pos.Perp()-rpos_wire[iLayer])<fabs(pos.Perp()-rpos_wire[iLayer+1])) {
      layerID = iLayer;
      break;
    } else if (iLayer==nLayer-1){
      layerID = iLayer;
      break;
    }
  }
  return layerID;
}

/// Find the nearest wire from hit position
int FindWire(int ilayer,TVector3 pos){
  int wirenum = 0;
  for (int iLayer = 0; iLayer < nLayer; iLayer++){
    if (iLayer>0) wirenum += GetNumberOfWirePerLayer(iLayer-1);
    if (iLayer==ilayer) break;
  }

  double xy0[2];
  GetWirePosition(wirenum, xy0, pos.Z());

  double phi_hit = pos.Phi();
  double phi0    = atan2(xy0[1], xy0[0]);
  double dphi    = two_pi/((double)GetNumberOfWirePerLayer(ilayer));
  double phi     = phi0;
  int wireID = -1;
  for (int iwire = wirenum; iwire < wirenum+GetNumberOfWirePerLayer(ilayer); iwire++) {
    if (fabs(phi_hit-phi)<dphi/2.) {
      wireID = iwire;
      break;
    } else if (fabs(phi_hit-phi-two_pi)<dphi/2.) {
      wireID = iwire;
      break;
    } else if (fabs(phi_hit-phi+two_pi)<dphi/2.) {
      wireID = iwire;
      break;
    }
    phi += dphi;
  }
  return wireID;
}

/// Calculate the wire position in XY from z position
void GetWirePosition (int iwire, double *xy, double zpos){
  xy[0] = fXYWire[iwire]->GetParameter(0)*zpos + fXYWire[iwire]->GetParameter(2);
  xy[1] = fXYWire[iwire]->GetParameter(1)*zpos + fXYWire[iwire]->GetParameter(3);
}

// fit procedure
void fittrack(int iev){
  /*std::cout << "//----------------------------------//" << std::endl;
  std::cout << "// Fitting for event "<< eventID <<" //" << std::endl;
  std::cout << "//   nHits1st = "<< nHits1st <<"     //" << std::endl;
  std::cout << "//   nLayers = "<< nLayers <<"       //" << std::endl;
  std::cout << "//   CHT status = "<< statusCTH <<"  //" << std::endl;
  std::cout << "//----------------------------------//" << std::endl;*/

  Track *fitTrack(NULL);
  Track *fitTracktrue(NULL);

  // define wire plane
  TVector3 wire0_1st, wire1_1st; // first wire coordination
  wire0_1st = hits[0].wire0;
  wire1_1st = hits[0].wire1;
  int detId = 1;

  TVector3 xyz0sm = xyz0;
  TVector3 mom0sm = mom0;

  // smearing initial parameters
  //for(int i=0;i<3;i++) xyz0sm[i]+=gRandom->Gaus(0,0.02); // ideal case
  for(int i=0;i<2;i++) xyz0sm[i]+=gRandom->Gaus(0,1.6);    // reasonable (1.6cm)
  xyz0sm[2]+=gRandom->Gaus(0,10);                          // reasonalbe (10cm)
  for(int i=0;i<3;i++) mom0sm[i]+=gRandom->Gaus(0,0.01);   // reasonalbe (10MeV)

  AbsTrackRep *rep  = new RKTrackRep(pdg);
  AbsTrackRep *rep0 = new RKTrackRep(pdg);

  // use for initialization
  MeasuredStateOnPlane state0(rep0);
  MeasuredStateOnPlane state0sm(rep);

  TMatrixDSym covM(6);
  TMatrixDSym covM0(6);

  // "QUESTION" position resolution to cov(0,0),cov(1,1),cov(2,2)? -> YES
  double resolution  = rsmear;   // CDC position resolution
  if (!resolution) resolution = 0.0001*centimeter; // apply small value if rsmear==0
  double resolution0 = 0.0001*centimeter; // very small value for true track points
  for (int i=0;i<2;i++){
    covM(i,i)  = resolution*resolution;
    covM0(i,i) = resolution0*resolution0;
  }
  covM(2,2)  = 0.25*0.25; // CDC position z resolution (2.5 mm)
  covM0(2,2) = resolution0*resolution0;

  // "QUESTION" momentum resolution to cov(3,3),cov(4,4),cov(5,5)? -> YES (10%)
  for (int i=3;i<5;i++){
    covM(i,i)  = 0.0005*0.0005;     // 500 keV for transversed momentum
    covM0(i,i) = 0.000001*0.000001; // small value for true track points
  }
  covM(5,5)  = 0.005*0.005;       // 5 MeV for longitudunal momentum
  covM0(5,5) = 0.000001*0.000001; // small value for true track points

  rep->setPosMomCov(state0sm, xyz0sm, mom0sm, covM);
  rep0->setPosMomCov(state0, xyz0, mom0, covM0);

  TVector3 poca, dirInPoca, poca_onwire;
  //rep0->extrapolateToLine(state0, wire0_1st, wire1_1st, poca, dirInPoca, poca_onwire);

  TVectorD seedState(6);
  TVectorD seedState0(6);
  TMatrixDSym seedCov(6);
  TMatrixDSym seedCov0(6);

  rep->get6DStateCov(state0sm, seedState, seedCov);
  rep0->get6DStateCov(state0, seedState0, seedCov0);

  //std::cout << "smeared initial state is... " << std::endl;
  //std::cout << seedState[0] << " " << seedState[1] << " " << seedState[2] << " : " << seedState[3] << " " << seedState[4] << " " << seedState[5] << std::endl;

  fitTrack     = new Track(rep,  seedState,  seedCov);
  fitTracktrue = new Track(rep0, seedState0, seedCov0);

  int nhits = hits.size();
  int nhitsadd = 0;

  std::vector<TrackPoint*>            trackPoints(nhits);
  std::vector<TrackPoint*>            trackPointstrue(nhits);
  std::vector<WireMeasurement*>       whits(nhits);
  std::vector<SpacepointMeasurement*> truehits(nhits);

  for(int i=0;i<nhits;i++){
    // hit wire
    TVector3 wire0, wire1;
    wire0 = hits[i].wire0;
    wire1 = hits[i].wire1;
    TVectorD wireConf(7);
    TMatrixDSym wireMatrix(7);

    wireConf[0] = wire0.X();
    wireConf[1] = wire0.Y();
    wireConf[2] = wire0.Z();
    wireConf[3] = wire1.X();
    wireConf[4] = wire1.Y();
    wireConf[5] = wire1.Z();
    wireConf[6] = hits[i].dist;   //  driftD smeared
    //wireConf[6] = hits[i].distmc;  //  driftDtrue

    // "QUESTION": what should be filled in wireMatrix ???
    resolution = 0.00001; // small value for the wire position (should be almost fixed ???)
    for (int j = 0; j < 6; j++) wireMatrix(j,j) = resolution*resolution;
    resolution = rsmear;  // spatial resolution assuming 200um for drift distance
    if (!resolution) resolution = 0.0001*centimeter; // apply small value if rsmear==0
    for (int j = 6; j < 7; j++) wireMatrix(j,j) = resolution*resolution;

    // true hits
    TVectorD hitpos(3);
    TMatrixDSym trueMatrix(3);
    hitpos[0] = hits[i].xyz.X();
    hitpos[1] = hits[i].xyz.Y();
    hitpos[2] = hits[i].xyz.Z();
    resolution = 0.0010;  // small value for true track (10um)
    for (int k = 0; k < 3; k++) trueMatrix(k,k) = resolution*resolution;

    trackPoints[i]     = new TrackPoint(fitTrack);
    trackPointstrue[i] = new TrackPoint(fitTracktrue);

    int hitId = i;
    whits[i]    = new WireMeasurement(wireConf, wireMatrix, detId, hitId, trackPoints[i]);
    truehits[i] = new SpacepointMeasurement(hitpos, trueMatrix, detId, hitId, trackPointstrue[i]);

    int leftright = (wire1-wire0).Cross(hits[i].mom).Dot(hits[i].xyz-wire0)>0?-1:1;
    whits[i]->setLeftRightResolution(leftright);

    trackPoints[i]->addRawMeasurement(whits[i]);
    trackPointstrue[i]->addRawMeasurement(truehits[i]);

    nhitsadd++;

    //std::cout << "before insert " << i << std::endl;
    fitTrack->insertPoint(trackPoints[i], hitId);
    fitTracktrue->insertPoint(trackPointstrue[i], hitId);
    //std::cout << "After insert " << i << std::endl;
  }

  //const genfit::eMultipleMeasurementHandling mmHandling = genfit::weightedAverage;
  //const genfit::eMultipleMeasurementHandling mmHandling = genfit::unweightedClosestToReference;
  //const genfit::eMultipleMeasurementHandling mmHandling = genfit::unweightedClosestToPrediction;
  //const genfit::eMultipleMeasurementHandling mmHandling = genfit::weightedClosestToReference;
  //const genfit::eMultipleMeasurementHandling mmHandling = genfit::weightedClosestToPrediction;
  const genfit::eMultipleMeasurementHandling mmHandling = genfit::unweightedClosestToReferenceWire;
  //const genfit::eMultipleMeasurementHandling mmHandling = genfit::unweightedClosestToPredictionWire;
  //const genfit::eMultipleMeasurementHandling mmHandling = genfit::weightedClosestToReferenceWire;
  //const genfit::eMultipleMeasurementHandling mmHandling = genfit::weightedClosestToPredictionWire;

  unsigned int maxIterations = 20;
  unsigned int minIterations =  4;

  //genfit::KalmanFitter *kalman = new genfit::KalmanFitter(maxIterations);
  //genfit::KalmanFitterRefTrack *kalman = new genfit::KalmanFitterRefTrack(maxIterations);
  genfit::AbsKalmanFitter *kalman = new genfit::DAF();
  kalman->setMinIterations(minIterations);
  kalman->setMultipleMeasurementHandling(mmHandling);

  //genfit::KalmanFitterRefTrack *kalman0 = new genfit::KalmanFitterRefTrack(maxIterations);
  genfit::AbsKalmanFitter *kalman0 = new genfit::DAF();
  kalman0->setMinIterations(minIterations);
  //kalman0->setMultipleMeasurementHandling(mmHandling); // true track fitting use a point measurement...

  //kalman->setDebugLvl(2);
  //kalman0->setDebugLvl(2);

  //std::cout << "fitTrack->getNumPoints: " << fitTrack->getNumPoints()
  //          << "| fitTrack->getNumPointsWithMeasurement: " << fitTrack->getNumPointsWithMeasurement()
  //          << "| nhits: " << nhits << std::endl;

  // fit wire measurement
  //try{
  //FitStatus* fitStatus0 = fitTrack->getFitStatus(rep);
  //double chi20 = fitStatus0->getChi2();
  //std::cout << "chi2 befire " << chi20 << std::endl;
  kalman->processTrackWithRep(fitTrack, rep);
  //kalman->processTrack(fitTrack);
  //FitStatus* fitStatus1 = fitTrack->getFitStatus(rep);
  //double chi21 = fitStatus1->getChi2();
  //std::cout << "chi2 after " << chi21 << std::endl;
  //}catch(Exception& e){e.what();}

  if (!fitTrack->getFitStatus(rep)->isFitted()) {
    std::cout << " Failed to fit the track..." << std::endl;
    statusFit   = 0;
    nHitsFitted = 0;
    outTree->Fill();

    delete kalman;
    delete kalman0;
    delete fitTrack;
    delete fitTracktrue;
    return;
  }

  // fit true track
  kalman0->processTrackWithRep(fitTracktrue, rep0);
  //display->addEvent(fitTrack);
  //display->addEvent(fitTracktrue);
  if (!fitTracktrue->getFitStatus(rep0)->isFitted()) {
    std::cout << " Failed to fit the true track..." << std::endl;
    statusFit   = -1;
    nHitsFitted = 0;
    outTree->Fill();

    delete kalman;
    delete kalman0;
    delete fitTrack;
    delete fitTracktrue;
    return;
  }

  statusFit   = 1;
  chi2ndf     = fitTrack->getFitStatus(rep)->getChi2()/fitTrack->getFitStatus(rep)->getNdf();
  pvalue      = fitTrack->getFitStatus(rep)->getPVal();
  nHitsFitted = nhits; // only for DAF case ???

  MeasuredStateOnPlane mop;
  MeasuredStateOnPlane mop0;
  TrackPoint* point = 0;
  for(int ihit = 0; ihit < nhits; ihit++){

    mcposX.push_back(hits[ihit].xyz.X()); 
    mcposY.push_back(hits[ihit].xyz.Y()); 
    mcposZ.push_back(hits[ihit].xyz.Z()); 
    mcposD.push_back(hits[ihit].distmc);
    mcmomX.push_back(1e3*hits[ihit].mom.X()); 
    mcmomY.push_back(1e3*hits[ihit].mom.Y()); 
    mcmomZ.push_back(1e3*hits[ihit].mom.Z()); 

    double wire_xy[2];
    int    wireID     = hits[ihit].wireID;
    double driftDtrue = hits[ihit].distmc;

    // get fitted status of fit track
    point = fitTrack->getPointWithFitterInfo(ihit, rep);
    TVector3    posonplane;
    TVector3    momonplane;
    TMatrixDSym covonplane(6);
    if (point != NULL) {
      mop = fitTrack->getFittedState(ihit,rep);
      mop.getPosMomCov(posonplane,momonplane,covonplane);
      recposX.push_back(posonplane.X());
      recposY.push_back(posonplane.Y());
      recposZ.push_back(posonplane.Z());
      recmomX.push_back(1e3*momonplane.X());
      recmomY.push_back(1e3*momonplane.Y());
      recmomZ.push_back(1e3*momonplane.Z());
      recposXCov.push_back(sqrt(covonplane[0][0]));
      recposYCov.push_back(sqrt(covonplane[1][1]));
      recposZCov.push_back(sqrt(covonplane[2][2]));
      recmomXCov.push_back(1e3*sqrt(covonplane[3][3]));
      recmomYCov.push_back(1e3*sqrt(covonplane[4][4]));
      recmomZCov.push_back(1e3*sqrt(covonplane[5][5]));
    } else {
      recposX.push_back(-1e9);
      recposY.push_back(-1e9);
      recposZ.push_back(-1e9);
      recmomX.push_back(-1e9);
      recmomY.push_back(-1e9);
      recmomZ.push_back(-1e9);
      recposXCov.push_back(-1e9);
      recposYCov.push_back(-1e9);
      recposZCov.push_back(-1e9);
      recmomXCov.push_back(-1e9);
      recmomYCov.push_back(-1e9);
      recmomZCov.push_back(-1e9);
    }

    // get fitted status of true hit track
    point = fitTracktrue->getPointWithFitterInfo(ihit, rep0);
    TVector3    posonplane0;
    TVector3    momonplane0;
    TMatrixDSym covonplane0(6);
    if (point != NULL) {
      mop0 = fitTracktrue->getFittedState(ihit,rep0);
      mop0.getPosMomCov(posonplane0,momonplane0,covonplane0);
      recposXtrue.push_back(posonplane0.X());
      recposYtrue.push_back(posonplane0.Y());
      recposZtrue.push_back(posonplane0.Z());
      recmomXtrue.push_back(1e3*momonplane0.X());
      recmomYtrue.push_back(1e3*momonplane0.Y());
      recmomZtrue.push_back(1e3*momonplane0.Z());
    } else {
      recposXtrue.push_back(-1e9);
      recposYtrue.push_back(-1e9);
      recposZtrue.push_back(-1e9);
      recmomXtrue.push_back(-1e9);
      recmomYtrue.push_back(-1e9);
      recmomZtrue.push_back(-1e9);
    }

    GetWirePosition(wireID, wire_xy, posonplane.Z());
    double driftDsmeared = hits[ihit].dist;
    double driftDfitted  = hypot((posonplane.X()-wire_xy[0]), (posonplane.Y()-wire_xy[1]));
    recposD.push_back(driftDfitted);

    GetWirePosition(wireID, wire_xy, posonplane0.Z());
    double driftDfittrue = hypot((posonplane0.X()-wire_xy[0]), (posonplane0.Y()-wire_xy[1]));
    recposDtrue.push_back(driftDfittrue);

    driftDdiff->Fill(driftDtrue-driftDfitted);
    driftDvsdriftres->Fill(driftDtrue,driftDtrue-driftDfitted);

    if(ihit==0){
      if(driftDtrue-driftDfitted>0.2) continue; // cut if residual is larger than 2mm

      double mom_fitted = sqrt(momonplane.X()*momonplane.X() + momonplane.Y()*momonplane.Y() + momonplane.Z()*momonplane.Z())*1e3;
      double mom_true   = sqrt(mom0[0]*mom0[0] + mom0[1]*mom0[1] + mom0[2]*mom0[2])*1e3;
      double momt       = sqrt(momonplane.X()*momonplane.X() + momonplane.Y()*momonplane.Y())*1e3;
      double momz       = momonplane.Z()*1e3;
      double momt_true  = sqrt(mom0[0]*mom0[0] + mom0[1]*mom0[1])*1e3;
      double momz_true  = mom0[2]*1e3;
      momthist->Fill(momt_true);
      momres->Fill((mom_fitted-mom_true));
      momtres->Fill((momt-momt_true));
      momzres->Fill((momz-momz_true));

      if(nHits1st>20 && nHits1st<=40) momres_20_40->Fill(mom_fitted-mom_true);
      if(nHits1st>40 && nHits1st<=50) momres_40_50->Fill(mom_fitted-mom_true);
      if(nHits1st>50 && nHits1st<=60) momres_50_60->Fill(mom_fitted-mom_true);
      if(nHits1st>60 && nHits1st<=70) momres_60_70->Fill(mom_fitted-mom_true);
      if(nHits1st>70 && nHits1st<=80) momres_70_80->Fill(mom_fitted-mom_true);
      if(nHits1st>80 && nHits1st<=90) momres_80_90->Fill(mom_fitted-mom_true);
    }
  }

  //FitStatus* fitStatus = fitTrack->getFitStatus(rep);
  //FitStatus* fitStatus = fitTracktrue->getFitStatus(rep0);

  //try{
  //  if(!fitStatus->isFitted()){
  //    rep->extrapolateToLine(state0sm, wire0_1st, wire1_1st, poca, dirInPoca, poca_onwire);
  //    fitStatus = fitTrack->getFitStatus(rep);
  //  }
  //}catch(Exception& e){e.what();}
  //if (!fitTrack->hasFitStatus()) std::cout << "Track is not fitted" << std::endl;

  chi2hist->Fill(chi2ndf);
  pvalhist->Fill(pvalue);
  outTree->Fill();

  delete kalman;
  delete kalman0;
  delete fitTrack;
  delete fitTracktrue;
}

void endfitter(){

  std::cout << "//----------------------------------//" << std::endl;
  std::cout << "// End of Fitter...                 //" << std::endl;
  std::cout << "//----------------------------------//" << std::endl;
  gStyle->SetOptFit(111);

  TCanvas *c1 = new TCanvas("c1","c1",10,10,600,800);
  TF1 *dgaus = new TF1("dgaus","gaus(0)+gaus(3)",-1.5,1.5);
  dgaus->SetParameters(200, 0.0, 0.15, 100, 0, 1.);
  dgaus->SetParLimits(1, -0.1, 0.1);
  dgaus->SetParLimits(4, -0.1, 0.1);

  c1->cd();
  momres->Fit(dgaus, "R");
  c1->Print(Form("momres_Z%d.eps",startZ));

  outputfile->cd();
  outTree->Write();
  momres->Write();
  momres_20_40->Write();
  momres_40_50->Write();
  momres_50_60->Write();
  momres_60_70->Write();
  momres_70_80->Write();
  momres_80_90->Write();
  momtres->Write();
  momzres->Write();
  chi2hist->Write();
  pvalhist->Write();
  driftDdiff->Write();
  driftDfittedhist->Write();
  driftDvsdriftres->Write();
  momthist->Write();
  Nhithist->Write();
  outputfile->Close();
}

void usage(){
  std::cout <<
  "Usage: fittracks [options] \n\
  Options:\n\
  -o, --output   <arg> set output file\n\
  -i, --input    <arg> output file \n\
  -g, --geometry <arg> geometry \n" << std::endl;
}

void clearTree(){
  //eventID      = -1;  // event ID
  //rsmear       = 200; // smearing value (given)
  //iniposZ      = 1e9; // at the target  (given)
  inimomX      = 1e9; // at the target (MC)
  inimomY      = 1e9; // at the target (MC)
  inimomZ      = 1e9; // at the target (MC)
  nHits        = -1;  // #of total hits
  nHits1st     = -1;  // #of hits in 1st turn
  nTurns       = -1;  // #of turns
  nLayers      = -1;  // #of layers
  statusCut    = -1;  // cut status (0: not selected, 1: selected)
  statusFit    = -1;  // fit status (0: not fitted, 1: fitted)
  statusCTH    = -1;  // cth status (0: no hit, 1: hit, 2: indirect hit)
  nHitsFitted  = -1;  // #of hits used for the fitting
  chi2ndf      = 1e9; // chi2/ndf
  pvalue       = 1e9; // p-value
  mcposX.clear();     // X hit point (MC)
  mcposY.clear();     // Y hit point (MC)
  mcposZ.clear();     // Z hit point (MC)
  mcposD.clear();     // D hit point (MC)
  mcmomX.clear();     // mom X hit point (MC)
  mcmomY.clear();     // mom Y hit point (MC)
  mcmomZ.clear();     // mom Z hit point (MC)
////
  recposX.clear();    // X hit point
  recposY.clear();    // Y hit point
  recposZ.clear();    // Z hit point
  recposD.clear();    // D hit point
  recmomX.clear();    // mom X hit point
  recmomY.clear();    // mom Y hit point
  recmomZ.clear();    // mom Z hit point
  recposXCov.clear(); // Cov for X hit point
  recposYCov.clear(); // Cov for Y hit point
  recposZCov.clear(); // Cov for Z hit point
  recmomXCov.clear(); // Cov for mom X hit point
  recmomYCov.clear(); // Cov for mom Y hit point
  recmomZCov.clear(); // Cov for mom Z hit point
////
  recposXtrue.clear();    // X hit point
  recposYtrue.clear();    // Y hit point
  recposZtrue.clear();    // Z hit point
  recposDtrue.clear();    // D hit point
  recmomXtrue.clear();    // mom X hit point
  recmomYtrue.clear();    // mom Y hit point
  recmomZtrue.clear();    // mom Z hit point
}

//----------------------------------------------------------//
//  main function
//----------------------------------------------------------//

int main(int argc, char **argv)
{
  gRandom->SetSeed(time(NULL));
  std::cout << "//----------------------------------//" << std::endl;
  std::cout << "// Start Genfit2 program \\(^O^)/    //" << std::endl;
  std::cout << "//----------------------------------//" << std::endl;
  static struct option long_options[] = {
    {"input"       , 1, 0, 'i'},
    {"geometry"    , 1, 0, 'g'},
    {"output"      , 1, 0, 'o'},
    {"resolution"  , 1, 0, 'r'},
    {0, 0, 0, 0}
  };
  /*int c;
  while(1){
    int option_index = 0;
    c = getopt_long(argc,argv,"i:o:g:r:e:n:",long_options, &option_index);
    if(c == -1) break;
    switch (c) {
      case 0: break;
      case 'i': if(optarg) input=optarg;             break;
      case 'g': if(optarg) geomfile=optarg;          break;
      case 'o': if(optarg) output=optarg;            break;
      default: usage(); break;
    }
  }*/

  TApplication app("app",&argc,argv);

  std::cout << "Input the target z position (-40 - 40, 5cm steps) : ";
  std::cin >> startZ;
  std::cout << "Input the smearing value for r (0, 100, 200, 300,...) : ";
  std::cin >> smearR;

  outputfile     = new TFile(Form("outputfile_Z%d_smear%dum.root",startZ,smearR),"RECREATE");
  outTree        = new TTree("outTree", "fit result");
  outTree->Branch("eventID",    &eventID);
  outTree->Branch("rsmear",     &rsmear);
  outTree->Branch("iniposZ",    &iniposZ); // target
  outTree->Branch("inimomX",    &inimomX); // target
  outTree->Branch("inimomY",    &inimomY); // target
  outTree->Branch("inimomZ",    &inimomZ); // target
  outTree->Branch("nHits",      &nHits);
  outTree->Branch("nHits1st",   &nHits1st);
  outTree->Branch("nTurns",     &nTurns);
  outTree->Branch("nLayers",    &nLayers);
  outTree->Branch("statusCut",  &statusCut);
  outTree->Branch("statusFit",  &statusFit);
  outTree->Branch("statusCTH",  &statusCTH);
  outTree->Branch("nHitsFitted",&nHitsFitted);
  outTree->Branch("chi2ndf",    &chi2ndf);
  outTree->Branch("pvalue",     &pvalue);
  outTree->Branch("mcmomX",     &mcmomX);
  outTree->Branch("mcmomY",     &mcmomY);
  outTree->Branch("mcmomZ",     &mcmomZ);
  outTree->Branch("mcposX",     &mcposX);
  outTree->Branch("mcposY",     &mcposY);
  outTree->Branch("mcposZ",     &mcposZ);
  outTree->Branch("mcposD",     &mcposD);
  outTree->Branch("recmomX",    &recmomX);
  outTree->Branch("recmomY",    &recmomY);
  outTree->Branch("recmomZ",    &recmomZ);
  outTree->Branch("recposX",    &recposX);
  outTree->Branch("recposY",    &recposY);
  outTree->Branch("recposZ",    &recposZ);
  outTree->Branch("recposD",    &recposD);
  outTree->Branch("recmomXCov", &recmomXCov);
  outTree->Branch("recmomYCov", &recmomYCov);
  outTree->Branch("recmomZCov", &recmomZCov);
  outTree->Branch("recposXCov", &recposXCov);
  outTree->Branch("recposYCov", &recposYCov);
  outTree->Branch("recposZCov", &recposZCov);
  outTree->Branch("recmomXtrue",&recmomXtrue);
  outTree->Branch("recmomYtrue",&recmomYtrue);
  outTree->Branch("recmomZtrue",&recmomZtrue);
  outTree->Branch("recposXtrue",&recposXtrue);
  outTree->Branch("recposYtrue",&recposYtrue);
  outTree->Branch("recposZtrue",&recposZtrue);
  outTree->Branch("recposDtrue",&recposDtrue);

  momres         = new TH1D("momres","momres",100,-1.5,1.5);
  momres_20_40   = new TH1D("momres_20_40","momres_20_40",100,-2.0,2.0);
  momres_40_50   = new TH1D("momres_40_50","momres_40_50",100,-2.0,2.0);
  momres_50_60   = new TH1D("momres_50_60","momres_50_60",100,-1.5,1.5);
  momres_60_70   = new TH1D("momres_60_70","momres_60_70",100,-1.5,1.5);
  momres_70_80   = new TH1D("momres_70_80","momres_70_80",100,-1.5,1.5);
  momres_80_90   = new TH1D("momres_80_90","momres_80_90",100,-1.5,1.5);

  momtres  = new TH1D("momtres","momtres",100,-1.5,1.5);
  momzres  = new TH1D("momzres","momzres",100,-5.0,5.0);
  chi2hist = new TH1D("chi2","chi2",100,0,7);
  pvalhist = new TH1D("pval","pval",100,0,1);

  driftDdiff       = new TH1D("driftDdiff","driftDdiff",100, -0.2 ,0.2);
  driftDfittedhist = new TH1D("driftDfittedhist","driftDfittedhist",100,0,1.6);
  driftDvsdriftres = new TH2D("driftDvsdriftres","driftDtrue vs driftDfitted",100,0,1.6,100,0,1.6);

  momthist = new TH1D("momthist","momthist",100,75,110);
  Nhithist = new TH1D("Nhithist","Nhithist",100,0,100);

  char  input[40];

  std::string output   = "out.root";
  std::string geomfile = "CyDetOnly.root";
  TChain *chain = new TChain("tree");
  chain->Add(Form("/home/yfujii/test_G4.10.01_z%d_Run0.root",startZ));
  std::cout << "Lord file: " << Form("/home/yfujii/test_G4.10.01_z%d_Run0.root",startZ) << std::endl;
  chain->Add(Form("/home/yfujii/test_G4.10.01_z%d_Run1.root",startZ));
  std::cout << "Lord file: " << Form("/home/yfujii/test_G4.10.01_z%d_Run1.root",startZ) << std::endl;

  TRint theApp("Rint", &argc, argv);
  TGeoManager *geo = TGeoManager::Import(geomfile.c_str());
  initfitter();

  int CdcCell_nHits    = 0;
  int CdcCell_nHits1st = 0;
  int CdcCell_nTurns   = 0;
  int CdcCell_maxLayer = 0;
  std::vector<double>  *CdcCell_x           = 0;
  std::vector<double>  *CdcCell_y           = 0;
  std::vector<double>  *CdcCell_z           = 0;
  std::vector<double>  *CdcCell_px          = 0;
  std::vector<double>  *CdcCell_py          = 0;
  std::vector<double>  *CdcCell_pz          = 0;
  std::vector<double>  *CdcCell_driftDtrue  = 0;
  std::vector<double>  *CdcCell_driftD      = 0;
  std::vector<int>     *CdcCell_cellID      = 0;
  std::vector<int>     *CdcCell_layerID     = 0;
  std::vector<double>  *CdcCell_edep        = 0;
  std::vector<int>     *CdcCell_hittype     = 0;
  std::vector<int>     *CdcCell_nturn       = 0;
  int CthHits_nHits  = 0;
  int CthHits_nHitsI = 0;
  double momvtxx = 1e9;
  double momvtxy = 1e9;
  double momvtxz = 1e9;

  int nev = chain->GetEntries();
  std::cout << "Total number of events = " << nev << std::endl;

  chain->SetBranchAddress("CdcCell_nHits",       &CdcCell_nHits     );
  chain->SetBranchAddress("CdcCell_nHits1st",    &CdcCell_nHits1st  );
  chain->SetBranchAddress("CdcCell_nTurns",      &CdcCell_nTurns    );
  chain->SetBranchAddress("CdcCell_maxLayer",    &CdcCell_maxLayer  );
  chain->SetBranchAddress("CdcCell_x",           &CdcCell_x         );
  chain->SetBranchAddress("CdcCell_y",           &CdcCell_y         );
  chain->SetBranchAddress("CdcCell_z",           &CdcCell_z         );
  chain->SetBranchAddress("CdcCell_px",          &CdcCell_px        );
  chain->SetBranchAddress("CdcCell_py",          &CdcCell_py        );
  chain->SetBranchAddress("CdcCell_pz",          &CdcCell_pz        );
  chain->SetBranchAddress("CdcCell_driftDtrue",  &CdcCell_driftDtrue);
  chain->SetBranchAddress("CdcCell_driftD",      &CdcCell_driftD    );
  chain->SetBranchAddress("CdcCell_cellID",      &CdcCell_cellID    );
  chain->SetBranchAddress("CdcCell_layerID",     &CdcCell_layerID   );
  chain->SetBranchAddress("CdcCell_edep",        &CdcCell_edep      );
  chain->SetBranchAddress("CdcCell_hittype",     &CdcCell_hittype   );
  chain->SetBranchAddress("CdcCell_nturn",       &CdcCell_nturn     );
  chain->SetBranchAddress("CthHits_nHits",       &CthHits_nHits     );
  chain->SetBranchAddress("CthHits_nHitsI",      &CthHits_nHitsI    );
  chain->SetBranchAddress("momvtxx",             &momvtxx           );
  chain->SetBranchAddress("momvtxy",             &momvtxy           );
  chain->SetBranchAddress("momvtxz",             &momvtxz           );

  int nofevent_throughCut = 0;
  int nofeventall = 0;

  rsmear  = ((double)smearR)*micrometer;
  iniposZ = (double)startZ;
  std::cout << " z = " << iniposZ << ", drift distance will be smeared by " << rsmear << " cm" << std::endl;
  for(int iev = 0; iev < nev; iev++){

    nofeventall++;
    eventID = iev;
    clearTree();

    //std::cout << "iev = " << iev << std::endl;
    if (iev%10000==0) std::cout << iev << " events processed out of " << nev << std::endl;
    chain->GetEntry(iev);

    //std::cout << "branch read" << std::endl;
    //int nhits     = CdcCell_nHits;
    //int nhits_cth = CthHits_nHits;
    int nhits     = CdcCell_nHits1st; // #of hits in 1st turn
    int nhits_cth = CthHits_nHitsI;   // indirect-hit
    double mom_t  = sqrt(CdcCell_px->at(0)*CdcCell_px->at(0)+CdcCell_py->at(0)*CdcCell_py->at(0));

    inimomX  = momvtxx;
    inimomY  = momvtxy;
    inimomZ  = momvtxz;
    nHits    = CdcCell_nHits;
    nHits1st = CdcCell_nHits1st;
    nTurns   = CdcCell_nTurns;
    nLayers  = CdcCell_maxLayer+1;

    Nhithist->Fill(nhits);

    if (nHits1st>18 && nLayer>3) {
      statusCut = 1;
    } else {
      statusCut = 0;
      outTree->Fill();  
      continue;
    }

    if (CthHits_nHitsI>0) {
      statusCTH = 2;
    } else if (CthHits_nHits>0) {
      statusCTH = 1;
    } else {
      statusCTH = 0;
      outTree->Fill();  
      continue;
    }

    // init hits..
    hits.clear();

    // fill hits...
    int wireID = -1;
    mom0 = TVector3(0,0,0);

    for(int ihit=0;ihit<nhits;ihit++){
      StrawHit hit;

      if (CdcCell_hittype->at(ihit)!=0) continue;
      if (CdcCell_nturn->at(ihit)>1)    continue; // use only 1st turn hits right now...

      wireID = GetWireNumber(CdcCell_layerID->at(ihit), CdcCell_cellID->at(ihit));
      //std::cout << "ihit = " << ihit << std::endl;
      hit.wireID  = wireID;
      hit.wire0   = wirePos0[wireID];
      hit.wire1   = wirePos1[wireID];
      //hit.dist    = CdcCell_driftD->at(ihit)*centimeter;     // already smeared by 200um
      hit.distmc  = CdcCell_driftDtrue->at(ihit)*centimeter; // MC true
      hit.dist    = CdcCell_driftDtrue->at(ihit)*centimeter + gRandom->Gaus(0,rsmear); // MC true

      hit.xyz     = TVector3(+CdcCell_x->at(ihit)*centimeter,
                             +CdcCell_y->at(ihit)*centimeter,
                             +CdcCell_z->at(ihit)*centimeter);

      hit.mom     = TVector3(+CdcCell_px->at(ihit),
                             +CdcCell_py->at(ihit),
                             +CdcCell_pz->at(ihit));

      if(mom0.Mag()<hit.mom.Mag()){
        mom0 = hit.mom;
        xyz0 = hit.xyz;
      }
      hits.push_back(hit);
    }
    nofevent_throughCut++;
    fittrack(iev);

    //std::cout << "fit end" << std::endl;
  }
  std::cout << "neventall = " << nofeventall << std::endl;
  std::cout << "neventthrougncut = " << nofevent_throughCut << std::endl;
  std::cout << "neventthroughfittingcut = " << momres->GetEntries() << std::endl;
  //display->open();
  endfitter();

  app.Run();
}
