#ifndef RECEVENT_H
#define RECEVENT_H 1
#include "TVector3.h"
#include "TVectorD.h"
#include "TMatrixDSym.h"
#include <math.h>
#include <iostream>

inline std::ostream& operator<<(std::ostream& out,const TVector3 &vec){
  out<<vec[0]<<" "<<vec[1]<<" "<<vec[2];
  return out;
}

struct StrawHit{
  StrawHit(){dist=0;distmc=0;};
  double dist;
  double distmc;
  TVector3 xyz;
  TVector3 mom;
  TVector3 wire0;
  TVector3 wire1;
  int label;
  int wireID;
  double   GetD() const{return dist;}
  unsigned GetLabel() const{return label;}
  unsigned GetWire() const{return label%100;}
  //can be replaced by geom
  double GetXLay() const{TVector3 wdir=wire1-wire0;return (fabs(wdir.X())>fabs(wdir.Y()))? wire0.Y():wire0.X();}
};

struct recEvent{
  int ev;
  TVector3 pos;
  TVector3 mom;
  double  chi2;
  int    nhits;
  int   status;
  int    nfail;
  TVector3 posMC;
  TVector3 momMC;
  TVector3 momStart;
  void clear(){
    ev     = -1;
    chi2   = -1;
    nhits  = -1;
    status = -1;
    nfail  =  0;
    pos    = TVector3();
    mom    = TVector3();
  }
};
#endif
